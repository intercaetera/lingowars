class Player {
  constructor(id, avatarName, username) {
    this.id = id;
    this.avatarName = avatarName;
    this.username = username;
    this.hp = 100;
    this.currentQuestionId = 1;
    this.correctAnswersNumber = 0;
    this.wrongAnwersNumber = 0;
    this.targetedPlayerId = null;
  }

  getDamage(damage) {
    this.hp -= Number(damage);
  }

  getPlayerData() {
    return {
      id: this.id,
      username: this.username,
      avatar: this.avatarName,
      hp: this.hp,
      correctAnswers: this.correctAnswersNumber,
      wrongAnswers: this.wrongAnwersNumber,
    };
  }

  increaseCorrectAnswersNumber() {
    this.correctAnswersNumber += 1;
  }

  increaseWrongAnswersNumber() {
    this.wrongAnwersNumber += 1;
  }

  isDead() {
    return this.hp <= 0;
  }
}

module.exports = {
  Player,
};
