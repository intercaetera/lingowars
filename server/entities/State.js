const { GameStates } = require('../models/types');

class GameState {
  constructor() {
    this.playerList = [];
    this.sourceWords = [];
    this.targetWords = [];
    this.gameState = GameStates.LOBBY;
  }
}

module.exports = {
  GameState,
};
