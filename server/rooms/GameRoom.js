const colyseus = require("colyseus");

const { Responses, Requests, GameStates } = require("../models/types");
const { shouldStartGame, shouldEndGame, checkIfPlayerIsDead } = require("./helpers");

const targetWordsDB = require("../db/english.json");
const sourceWordsDB = require("../db/polish.json");

const { Player } = require("../entities/Player");
const { GameState } = require("../entities/State");

const initialize = require("../controller/initialize");
const generate = require("../controller/generator");
const checker = require("../controller/checker");

const config = require('../config');

exports.GameRoom = class extends colyseus.Room {
  constructor() {
    super();
  }

  onCreate() {
    const { source, target } = initialize(sourceWordsDB, targetWordsDB, {});

    this.maxClients = config.MIN_PLAYERS;
    this.gameState = new GameState();

    this.gameState.sourceWords = source;
    this.gameState.targetWords = target;
  }

  async onJoin(client, data) {
    const { id } = client;
    const { username, avatarName } = data;

    const player = new Player(id, avatarName, username);

    this.gameState.playerList.push(player);

    this.sendResponse(client, {
      type: Responses.JOINING_SUCCESSFUL,
      payload: this.gameState.playerList
    });

    this.broadcastResponse({
      type: Responses.USER_JOINED,
      payload: player
    });

    const shouldGameStart = shouldStartGame(this.gameState.playerList);

    if (shouldGameStart) {
      this.gameState.gameState = GameStates.STARTED;

      setTimeout(() => {
        this.lock();

        this.broadcastResponse({
          type: Responses.START_GAME,
        });
      }, config.DEFAULT_START_TIMEOUT);
    }

    return;
  }

  onMessage(client, request) {
    this.requestHandler(client, request);

    const deadPlayers = checkIfPlayerIsDead(this.gameState.playerList);

    deadPlayers.forEach(player => {
      this.sendResponse(client, {
        type: Responses.PLAYER_IS_DEAD,
        payload: player,
      });
    });

    const winner = shouldEndGame(this.gameState.playerList);

    if (winner) {
      this.broadcastResponse({
        type: Responses.END_GAME,
        payload: {
          winner,
          playerList: this.gameState.playerList,
        }
      });

      this.gameState = new GameState();
      this.gameState.gameState = GameStates.ENDED;
      return;
    }

    if (!this.gameState.gameState === GameStates.ENDED) {
      return;
    }

    this.broadcastResponse({
      type: Responses.BATTLE_TICK,
      payload: this.gameState.playerList
    });
  }

  onLeave(client) {
    const currentPlayer = this.gameState.playerList.find(
      player => player.id === client.id,
    );

    const newPlayerList = this.gameState.playerList.filter(
      player => player.id !== currentPlayer.id
    );

    this.gameState.playerList = newPlayerList;

    if (this.gameState.gameState === GameStates.STARTED) {
      this.broadcastResponse({
        type: Responses.USER_LEFT_WHILE_GAME_STARED,
        payload: currentPlayer
      });

      currentPlayer.hp = 0;

      return;
    }

    this.broadcastResponse({
      type: Responses.USER_LEFT,
      payload: currentPlayer
    });
  }

  sendResponse(client, response) {
    this.send(client, response);
  }

  broadcastResponse(response) {
    this.broadcast(response);
  }

  requestHandler(client, request) {
    if (!request.type) {
      return;
    }

    switch (request.type) {
      case Requests.SEND_PLAYER_DATA: {
        const player = this.gameState.playerList.find(player => player.id === client.id);

        if (!player) {
          return;
        }

        const playerData = player.getPlayerData();

        this.sendResponse(client, {
          type: Responses.PLAYER_DATA_SENT,
          payload: playerData,
        });

        break;
      }

      case Requests.SEND_ANSWER: {
        const { questionId, answer } = request.payload;

        const player = this.gameState.playerList.find(player => player.id === client.id);

        if (!player) {
          return;
        }

        const check = checker({
          sentence: answer,
          questionId,
          target: this.gameState.targetWords,
        });

        if (!check.wasCorrect) {
          player.increaseWrongAnswersNumber();
          player.getDamage(config.DEFAULT_ANSWER_DAMAGE);
        }

        if (check.wasCorrect) {
          player.increaseCorrectAnswersNumber();
        }

        this.sendResponse(client, {
          type: Responses.SEND_FEEDBACK,
          payload: {
            wasCorrect: check.wasCorrect,
            correctAnswer: check.correctAnswer,
          },
        });

        break;
      }

      case Requests.GET_QUESTION: {
        const player = this.gameState.playerList.find(player => player.id === client.id);

        if (!player) {
          return;
        }

        const questionId = player.currentQuestionId;

        const question = generate({
          questionId,
          source: this.gameState.sourceWords,
          target: this.gameState.targetWords,
        });

        player.currentQuestionId += 1;

        this.sendResponse(client, {
          type: Responses.GET_QUESTION_RESPONSE,
          payload: question,
        });

        break;
      }

      case Requests.DEAL_DAMAGE_TO_PLAYER: {
        const { targetedPlayerId } = request.payload;
        const targetedPlayer = this.gameState.playerList.find(player => player.id === targetedPlayerId);

        if (!targetedPlayer) {
          return;
        }

        targetedPlayer.getDamage(config.DEFAULT_DEALED_DAMAGE);

        this.sendResponse({
          type: Responses.PLAYER_RECEIVED_DAMAGE,
          payload: targetedPlayerId,
        });

        break;
      }
    }
  }
};
