const config = require('../config');

const shouldStartGame = playerList => playerList.length === config.MIN_PLAYERS;

const shouldEndGame = playerList => {
  const winner = playerList.filter(player => !player.isDead());

  if (winner.length !== 1) {
    return;
  }

  return winner[0];
};

const checkIfPlayerIsDead = playerList => {
  return playerList.filter(player => player.isDead());
}

module.exports = {
  shouldStartGame,
  shouldEndGame,
  checkIfPlayerIsDead,
};
