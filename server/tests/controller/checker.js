/* eslint-env node, mocha */
const { expect } = require('chai');

const checker = require('../../controller/checker');

const target = ['apple', 'pear', 'orange'];

describe('checker', () => {
  it('should return true if source sentence matches target', () => {
    const expected = {
      isValid: true,
      correctAnswer: 'apple',
    };

    const actual = checker({
      answer: 'apple',
      questionId: 0,
      target,
    });

    expect(expected).to.deep.equal(actual);
  });

  it('should return string if source sentence doesnt match target', () => {
    const expected = {
      isValid: false,
      correctAnswer: 'apple',
    };

    const actual = checker({
      answer: 'deliberatelywrong',
      questionId: 0,
      target,
    });

    expect(expected).to.deep.equal(actual);
  });
});
