/* eslint-env node, mocha */
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const deepEqualInAnyOrder = require('deep-equal-in-any-order');

chai.use(sinonChai);
chai.use(deepEqualInAnyOrder);

const { expect } = chai;

const generator = require('../../controller/generator');

const { randomExcept } = generator;

describe('randomExcept', () => {
  it('should generate a number thats not a disallowed number', () => {
    let counter = 0;
    const spy = sinon.spy();
    const randomFunction = () => {
      spy();
      return counter++;
    };

    const expected = 1;
    const actual = randomExcept([0], randomFunction);

    expect(expected).to.equal(actual);
    expect(spy).to.have.been.calledTwice;
  });
});

describe('generator', () => {
  it('should return a snapshot response', () => {
    const source = ['jablko'];
    const target = ['apple', 'orange', 'plum', 'grape'];

    const prompt = 'jablko';
    const questionId = 0;

    const actual = generator({ questionId, source, target });

    expect(actual.prompt).to.equal(prompt);
    expect(actual.answers).to.deep.equalInAnyOrder(target);
    expect(actual.questionId).to.equal(questionId);
  });
});
