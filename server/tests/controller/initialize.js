/* eslint-env node, mocha */
const { expect } = require('chai');

const initialize = require('../../controller/initialize');

describe('initialize', () => {
  describe('link', () => {
    it('should correctly link arrays', () => {
      const source = ['x', 'y', 'z'];
      const target = ['a', 'b', 'c'];

      const expected = [
        { source: 'x', target: 'a' },
        { source: 'y', target: 'b' },
        { source: 'z', target: 'c' },
      ];

      const actual = initialize.linkSourceAndTarget(source, target);

      expect(actual).to.deep.equal(expected);
    });
  });

  describe('unlink', () => {
    it('should correctly unlink arrays', () => {
      const linked = [
        { source: 'x', target: 'a' },
        { source: 'y', target: 'b' },
        { source: 'z', target: 'c' },
      ];

      const expected = {
        source: ['x', 'y', 'z'],
        target: ['a', 'b', 'c'],
      };

      const actual = initialize.unlinkSourceAndTarget(linked);

      expect(actual).to.deep.equal(expected);
    });
  });

  describe('initialize', () => {
    it('given a function should apply it exactly to two arrays', () => {
      const shuffleFunction = array => array.reverse();

      const source = ['x', 'y', 'z'];
      const target = ['a', 'b', 'c'];

      const expected = {
        source: ['z', 'y', 'x'],
        target: ['c', 'b', 'a'],
      };

      const actual = initialize(source, target, { shuffleFunction });

      expect(actual).to.deep.equal(expected);
    });

    it('should not throw if the last argument is empty', () => {
      const source = ['x', 'y', 'z'];
      const target = ['a', 'b', 'c'];

      const func = () => initialize(source, target);

      expect(func).to.not.throw();
    });
  });
});
