const http = require('http');
const express = require('express');
const cors = require('cors');
const colyseus = require('colyseus');
const monitor = require('@colyseus/monitor').monitor;

const GameRoom = require('./rooms/GameRoom').GameRoom;

const port = process.env.PORT || 2567;
const serverPort = 5000;
const app = express();

app.use(cors());
app.use(express.json());

app.use('/assets', express.static('assets'));

const server = http.createServer(app);

const gameServer = new colyseus.Server({
  server,
  express: app,
});

gameServer.define('main_room', GameRoom);

app.use('/colyseus', monitor(gameServer));

gameServer.listen(port);
app.listen(serverPort);
console.log(`Listening on ws://localhost:${ port }`);
