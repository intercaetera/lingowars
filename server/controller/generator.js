const config = require('../config');
const defaultSource = require('../db/polish.json');
const defaultTarget = require('../db/english.json');

const defaultRandom = upperBound => Math.floor(Math.random() * upperBound);

const defaultRandomExcept = (except, randomFunction) => {
  const value = randomFunction();

  if (except.includes(value)) {
    return defaultRandomExcept(except, randomFunction);
  } else {
    return value;
  }
};

const generator = ({
  questionId,
  source = defaultSource,
  target = defaultTarget,
  random = defaultRandom,
  randomExcept = defaultRandomExcept,
  numberOfAnswers = config.NUMBER_OF_ANSWERS,
}) => {
  const prompt = source[questionId];
  const correctAnswer = target[questionId];

  const wrongIndexes = [...Array(numberOfAnswers - 1)]
    .reduce((acc, _, index) => {
      acc[index] = randomExcept([...acc, questionId], () => random(target.length));
      return acc;
    }, []);

  const wrongAnswers = wrongIndexes.map(index => target[index]);

  const promptIndex = random(numberOfAnswers);

  const answers = [
    ...wrongAnswers.slice(0, promptIndex),
    correctAnswer,
    ...wrongAnswers.slice(promptIndex),
  ];

  return {
    prompt,
    answers,
    questionId,
  };
};

generator.randomExcept = defaultRandomExcept;

module.exports = generator;
