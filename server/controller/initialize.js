const shuffle = require('shuffle-array');

const linkSourceAndTarget = (source, target) =>
  source.map((sourceElement, index) => {
    const targetElement = target[index];

    return {
      source: sourceElement,
      target: targetElement,
    };
  });

const unlinkSourceAndTarget = linked =>
  linked.reduce((unlinked, element, index) => {
    unlinked.source[index] = element.source;
    unlinked.target[index] = element.target;
    return unlinked;
  }, { source: [], target: [] });

const defaultShuffle = array => shuffle(array, { copy: true });

const initialize = (source, target, {
  linkFunction = linkSourceAndTarget,
  shuffleFunction = defaultShuffle,
  unlinkFunction = unlinkSourceAndTarget,
} = {}) => {
  const linked = linkFunction(source, target);
  const shuffled = shuffleFunction(linked);
  const unlinked = unlinkFunction(shuffled);

  return unlinked;
};

initialize.linkSourceAndTarget = linkSourceAndTarget;
initialize.unlinkSourceAndTarget = unlinkSourceAndTarget;

module.exports = initialize;
