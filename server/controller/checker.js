const defaultTarget = require('../db/english.json');

const checker = ({
  sentence,
  questionId,
  target = defaultTarget,
}) => {
  const targetSentence = target[questionId];

  if (sentence === targetSentence) {
    return {
      wasCorrect: true,
      correctAnswer: targetSentence,
    };
  } else {
    return {
      wasCorrect: false,
      correctAnswer: targetSentence,
    };
  }
};

module.exports = checker;
