## CRA boilerplate

1. `react-redux`
2. `redux-devtools-extension`
3. `redux-saga`
4. `react-router-dom`
5. `immer`
6. `styled-components`
7. `antd`

## How to
1. Define routes in `src/App.js`
2. Edit/add your own reducers in `src/redux/reducers/default.js`
3. Edit/add your own sagas in `src/redux/sagas/default.js`
4. Use `styled-components` instead of CSS/SASS/SCSS/LESS etc.
5. Use `immer` to produce state in Redux
6. Read and follow https://hackernoon.com/fractal-a-react-app-structure-for-infinite-scale-4dab943092af