const getMessages = (locale = 'en') => {
  const def = {
    en: require('internationalization/en-US.json'),
    pl: require('internationalization/pl-PL.json'),
  };

  return def[locale];
};

export default getMessages;
