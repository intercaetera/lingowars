import hulk from "./assets/hulk.png";
import dude from "./assets/dude.png";
import shroom from "./assets/shroom.png";

export const avatarSprites = {
  hulk,
  dude,
  shroom
};

export const SERVER_URL = "nikodemwrona.com";
