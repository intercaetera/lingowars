import * as Colyseus from "colyseus.js";
import { SERVER_URL } from "../globals";

const client = new Colyseus.Client(`wss://${SERVER_URL}`);

let singletonRoom = null;

const removeRoom = () => {
  singletonRoom = null;
};

const getRoom = (username, avatarName) => {
  if (singletonRoom) {
    return Promise.resolve(singletonRoom);
  }

  return new Promise((resolve, reject) => {
    client
      .joinOrCreate("main_room", { username, avatarName })
      .then(room => {
        singletonRoom = room;
        resolve(singletonRoom);
      })
      .catch(e => {
        console.error("join error", e);
        Promise.reject(e);
      });
  });
};

export { getRoom, singletonRoom, removeRoom };
