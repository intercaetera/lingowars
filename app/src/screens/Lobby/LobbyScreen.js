import React, { PureComponent } from "react";
import { connect } from "react-redux";
import {} from "nes-react";
import { setUsername } from "./redux/actions";

import Character from "components/Character";

import "./styles.scss";

const PLAYERS_IN_ROOM = 4;

class LobbyScreen extends PureComponent {
  componentDidUpdate(prevProps) {
    if (this.props.gameStarted && !prevProps.gameStarted) {
      this.props.history.push("/battle");
    }
  }

  render() {
    const { playersList } = this.props;
    const emptySlots = PLAYERS_IN_ROOM - playersList.length;
    return (
      <div className="screen_wrapper">
        <h3 className="header">Waiting for other players</h3>
        <div className="lobby_users_wrapper">
          {playersList.map(player => (
            <div key={`avatar-${player.id}`} className="lobby_user_wrapper">
              <div className="lobby_user_avatar">
                <Character avatarName={player.avatarName} />
              </div>
              <div className="lobby_user_name">{player.username}</div>
            </div>
          ))}
          {new Array(emptySlots)
            .fill({ username: "waiting..." })
            .map((player, index) => (
              <div
                key={`key-${player.username}-${index}`}
                className="lobby_user_wrapper"
              >
                <div className="lobby_user_avatar">?</div>
                <div className="lobby_user_name">{player.username}</div>
              </div>
            ))}
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    username: state.common.username,
    playersList: state.common.playersList,
    gameStarted: state.common.gameStarted
  }),
  {
    setUsername
  }
)(LobbyScreen);
