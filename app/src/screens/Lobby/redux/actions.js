import { SET_USERNAME } from '../../../redux/actions';

export const setUsername = username => ({
  type: SET_USERNAME,
  payload: {
    username,
  }
});
