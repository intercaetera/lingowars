import React from "react";
import { connect } from "react-redux";
import { Button } from "nes-react";
import { getPlayerAvatarName, selectors } from "redux/selectors";
import "./styles.scss";

import Character from "components/Character";

class EndgameScreen extends React.PureComponent {
  state = {
    avatarName: "dude",
  }

  componentDidMount() {
    const { avatarName } = this.props;
    this.setState({ avatarName });
  }

  goToDashboard = () => {
    this.props.history.push("/play");
  };

  render() {
    const {
      winner,
      correctAnswersNumber,
      wrongAnwersNumber
    } = this.props;
    const { avatarName } = this.state;

    const status = winner ? (
      <h3 className="EndgameScreenStatus winner">Victory Achieved</h3>
    ) : (
      <h2 className="EndgameScreenStatus loser">You Died</h2>
    );

    const characterPicture = winner ? (
      <div className="EndgameScreenCharacter winner">
        <Character avatarName={avatarName} scale={1} />
      </div>
    ) : (
      <div className="EndgameScreenCharacter loser">
        <Character avatarName={avatarName} scale={1} />
      </div>
    );

    return (
      <div className="EndgameScreen">
        {status} {characterPicture}
        <Button onClick={this.goToDashboard}>Return to Dashboard</Button>
        <div className="EndgameDetails">
          <table>
            <tbody>
              <tr>
                <td>Correct</td>
                <td>{correctAnswersNumber}</td>
              </tr>
              <tr>
                <td>Wrong</td>
                <td>{wrongAnwersNumber}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  avatarName: getPlayerAvatarName(state),
  winner: state.common.winner,
  correctAnswersNumber: selectors.getNumberOfCorrect(state),
  wrongAnwersNumber: selectors.getNumberOfWrong(state)
}))(EndgameScreen);
