import React from "react";
import { connect } from "react-redux";
import { Icon } from "nes-react";
import "screens/Play/style.scss";
import { actions } from "screens/Battle/reduxStuff";
import "screens/Battle/BattleScreen.scss";
import Character from "../../components/Character";

import arrowIndicator from "assets/arrow.png";
import crosshair from "assets/crosshair.png";

const tilesRefs = {};

class Battlefield extends React.PureComponent {
  componentDidMount() {
    const { playersList, playerID } = this.props;
    const withoutUser = playersList.filter(player => player.id !== playerID);
    const randomPlayerId =
      withoutUser[Math.floor(Math.random() * withoutUser.length)].id;
    this.props.setFocus(randomPlayerId, tilesRefs);
  }

  getTilesCoords = (ref, player) => {
    if (!ref) return;
    tilesRefs[player.id] = {
      x: ref.offsetTop + ref.clientWidth / 3,
      y: ref.offsetLeft + ref.clientHeight / 3 + 10
    };
  };

  focusPlayer = player => {
    const { playerID } = this.props;
    if (player.id !== playerID) {
      this.props.setFocus(player.id);
    }
  };

  render() {
    const { playersList, playerID, focusedPlayerId } = this.props;
    return (
      <div className="BattlefieldGrid">
        <div id="FireballsWrapper" />
        {playersList.map(player => (
          <div
            className={`BattlefieldPlayer`}
            ref={ref => this.getTilesCoords(ref, player)}
            onClick={() => this.focusPlayer(player)}
            key={player.id}
          >
            <div
              className={`BattlefieldPlayerAvatar ${
                player.id === focusedPlayerId ? "focused" : ""
              }`}
            >
              {player.id === focusedPlayerId && (
                <img src={crosshair} alt="Crosshair" className="Crosshair" />
              )}
              {player.id === playerID && (
                <img
                  src={arrowIndicator}
                  alt="Arrow indicator"
                  className="ArrowIndicator"
                />
              )}
              <Character
                className={`PlayerAvatar`}
                avatarName={player.hp > 0 ? player.avatarName : "skull"}
              />
            </div>
            <div className="PlayerName">{player.username}</div>
            <div className="PlayerHealthWrapper">
              <Icon className="HeartIcon" icon="heart" small />
              <div
                style={{ width: `${(player.hp / 100) * 100}%` }}
                className="PlayerHealth"
              />
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default connect(
  state => ({
    playersList: state.common.playersList,
    playerID: state.common.playerID
  }),
  {
    dealDamage: actions.dealDamage
  }
)(Battlefield);
