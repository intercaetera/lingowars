/* eslint-disable no-param-reassign */

import produce from "immer";
import { takeLatest, put, delay } from "redux-saga/effects";
import { singletonRoom } from "services/colyseusService";

const colysActions = {
  GET_QUESTION: "GET_QUESTION",
  SEND_ANSWER: "SEND_ANSWER",
  DEAL_DAMAGE_TO_PLAYER: "DEAL_DAMAGE_TO_PLAYER",
  END_GAME: "END_GAME",
};

// redux
const actions = {
  RETRIEVE_QUESTION: "RETRIEVE_QUESTION",
  RECEIVED_QUESTION: "RECEIVED_QUESTION",
  SEND_ANSWER: "SEND_ANSWER",
  RECEIVED_FEEDBACK: "RECEIVED_FEEDBACK",
  DEAL_DAMAGE: "DEAL_DAMAGE_TO_PLATER",
  END_GAME: "END_GAME",

  sendAnswer: (questionId, answer) => ({
    type: actions.SEND_ANSWER,
    payload: {
      questionId,
      answer
    }
  }),
  dealDamage: toPlayer => ({
    type: actions.DEAL_DAMAGE,
    payload: {
      targetedPlayerId: toPlayer,
    }
  }),
  retrieveQuestion: () => ({ type: actions.RETRIEVE_QUESTION }),
};

const initialState = {
  prompt: {
    prompt: "",
    answers: ["", "", "", ""]
  },
  feedbackResult: {
    wasCorrect: undefined,
    correctAnswer: undefined
  },
  userAnswer: null,
};

const handleReceiveQuestion = (state, action) => {
  state.prompt = action.payload;
  state.feedbackResult = initialState.feedbackResult;
  state.userAnswer = initialState.userAnswer;
};

const handleReceiveFeedback = (state, action) => {
  state.feedbackResult = action.payload;
};

const handleSendAnswer = (state, action) => {
  state.userAnswer = action.payload.answer;
};


export default function(state = initialState, action) {
  const reducers = {};

  reducers[actions.RECEIVED_QUESTION] = handleReceiveQuestion;
  reducers[actions.RECEIVED_FEEDBACK] = handleReceiveFeedback;
  reducers[actions.SEND_ANSWER] = handleSendAnswer;

  if (!action || !action.type) {
    return state;
  }

  if (!reducers[action.type]) {
    return state;
  }

  return produce(state, draft => reducers[action.type](draft, action));
}

function handleSagaRetrieveQuestion() {
  console.log("i am asking for q");
  singletonRoom.send({ type: colysActions.GET_QUESTION });
}

function handleSagaSendAnswer({ payload }) {
  console.log("sending answer", payload);
  singletonRoom.send({ type: colysActions.SEND_ANSWER, payload });
}

function handleSagaDealDamage({ payload }) {
  singletonRoom.send({ type: colysActions.DEAL_DAMAGE_TO_PLAYER, payload });
}

function* handleSagaReceiveFeedback({ payload }) {
  if (payload.wasCorrect) {
    yield delay(500);
  } else {
    yield delay(2000);
  }

  yield put({ type: actions.RETRIEVE_QUESTION });
}

function* battleSaga() {
  yield takeLatest(actions.RETRIEVE_QUESTION, handleSagaRetrieveQuestion);
  yield takeLatest(actions.SEND_ANSWER, handleSagaSendAnswer);
  yield takeLatest(actions.DEAL_DAMAGE, handleSagaDealDamage);
  yield takeLatest(actions.RECEIVED_FEEDBACK, handleSagaReceiveFeedback);
}

export { actions, battleSaga };
