import React from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import { Redirect } from "react-router-dom";
import { Button, Container, Progress } from "nes-react";
import "screens/Play/style.scss";
import { actions } from "screens/Battle/reduxStuff";
import "screens/Battle/BattleScreen.scss";
import { selectors } from "redux/selectors";
import Battlefield from "./Battlefield";

const timerInitialValue = 5;

let tilesRefs = null;

class BattleScreen extends React.PureComponent {
  state = {
    focusedPlayerId: null,
    timer: timerInitialValue
  };

  startTimer = () => {
    this.setState({
      timer: timerInitialValue
    });

    this.timer = setInterval(() => {
      this.setState(state => ({
        timer: state.timer - 1
      }));
    }, 1000);
  };

  componentDidMount() {
    this.startTimer();
    const { retrieveQuestion } = this.props;
    retrieveQuestion();
  }

  componentDidUpdate(prevProps) {
    if (this.props.prompt.prompt !== prevProps.prompt.prompt) {
      clearInterval(this.timer);
      // new question!
      this.startTimer();
    }
    if (this.props.endGame && !prevProps.endGame) {
      this.props.history.push("/end-game");
    }

    const { prompt } = this.props;
    const { questionId } = prompt;
    if (this.state.timer < 1) {
      this.setState({
        timer: 10
      });
      clearInterval(this.timer);
      this.handleSendAnswer(questionId, "748234782648732");
    }
  }

  handleSendAnswer = (questionId, answer) => {
    const { userAnswer } = this.props;
    // do not allow changing anwsers
    if (userAnswer && userAnswer.length > 0) {
      return;
    }
    clearInterval(this.timer);
    const { sendAnswer } = this.props;
    sendAnswer(questionId, answer);
  };

  setFocus = (playerId, tilesRefs) => {
    this.setState({
      focusedPlayerId: playerId
    });
    tilesRefs = tilesRefs;
  }

  attackPlayer() {
    const { focusedPlayerId } = this.state;
    this.props.dealDamage(focusedPlayerId);
  }

  determineSuccess = (userAnswer, answer, feedbackResult) => {
    if (feedbackResult.wasCorrect && answer === userAnswer) {
      this.attackPlayer();
      return true;
    }

    if (
      feedbackResult.wasCorrect === false &&
      answer === feedbackResult.correctAnswer
    ) {
      return true;
    }
  };

  determineError = (userAnswer, answer, feedbackResult) => {
    // case when user has chosen an answer but it was wrong
    if (feedbackResult.wasCorrect === false && answer === userAnswer) {
      return true;
    }
  };

  render() {
    const { focusedPlayerId, timer } = this.state;
    const { prompt, userAnswer, answerFeedback, iAmDead } = this.props;
    const { answers, questionId, prompt: promptQuestion } = prompt;

    if (iAmDead) {
      return <Redirect to="/end-game" />;
    }

    if (answerFeedback.wasCorrect === false) {
      if (window.navigator && window.navigator.vibrate) {
        window.navigator.vibrate(500);
      }
    }

    const timeoutClass = classnames("TimeLeftContainer", {
      hidden: (userAnswer && userAnswer.length > 0) || timer === null
    });

    return (
      <div className="BattleScreen">
        <Battlefield
          setFocus={this.setFocus}
          focusedPlayerId={focusedPlayerId}
        />
        <div className="PlayContainer">
          <div className="QuestionContainer">
            <Container>{promptQuestion}</Container>
          </div>
          <div className={timeoutClass}>
            <Progress value={timer} max={timerInitialValue} error />
          </div>
          <div className="AnswersContainer">
            <Button
              success={this.determineSuccess(
                userAnswer,
                answers[0],
                answerFeedback
              )}
              error={this.determineError(
                userAnswer,
                answers[0],
                answerFeedback
              )}
              onClick={() => this.handleSendAnswer(questionId, answers[0])}
            >
              {answers[0]}
            </Button>
            <Button
              success={this.determineSuccess(
                userAnswer,
                answers[1],
                answerFeedback
              )}
              error={this.determineError(
                userAnswer,
                answers[1],
                answerFeedback
              )}
              onClick={() => this.handleSendAnswer(questionId, answers[1])}
            >
              {answers[1]}
            </Button>
            <Button
              success={this.determineSuccess(
                userAnswer,
                answers[2],
                answerFeedback
              )}
              error={this.determineError(
                userAnswer,
                answers[2],
                answerFeedback
              )}
              onClick={() => this.handleSendAnswer(questionId, answers[2])}
            >
              {answers[2]}
            </Button>
            <Button
              success={this.determineSuccess(
                userAnswer,
                answers[3],
                answerFeedback
              )}
              error={this.determineError(
                userAnswer,
                answers[3],
                answerFeedback
              )}
              onClick={() => this.handleSendAnswer(questionId, answers[3])}
            >
              {answers[3]}
            </Button>
          </div>
          <div className="StrategyContainer">
            <Button>STRATEGY 1</Button>
            <Button>STRATEGY 2</Button>
            <Button>STRATEGY 3</Button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    prompt: selectors.getPrompt(state),
    userAnswer: selectors.getUserAnswer(state),
    answerFeedback: selectors.getFeedback(state),
    endGame: state.common.endGame,
    iAmDead: selectors.getIfIamDead(state)
  }),
  {
    retrieveQuestion: actions.retrieveQuestion,
    sendAnswer: actions.sendAnswer,
    dealDamage: actions.dealDamage
  }
)(BattleScreen);
