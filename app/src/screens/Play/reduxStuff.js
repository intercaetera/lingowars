/* eslint-disable no-param-reassign */

import produce from "immer";
import _ from "lodash";
import { takeLatest } from "redux-saga/effects";
import { actions } from 'redux/sagas/default';

// redux

const initialState = {
  playerID: null
};

// const increaseCurrentNumber = (state) => {
//   state.currentNumber += 1;
// };

export default function(state = initialState, action) {
  const reducers = {};

  // reducers[INCREASE_NUMBER] = increaseCurrentNumber;

  if (!action || !action.type) {
    return state;
  }

  if (!reducers[action.type]) {
    return state;
  }

  return produce(state, draft => reducers[action.type](draft, action));
}

// selectors

const selectors = {
  getPlayerId: state => _.get(state, "common.playerID"),
  roomIsReady: state => _.get(state, "common.roomReady")
};

function* handleSagaRequestPlayerData() {
  // zmien to plz
  // getRoom().then(room => room.send({ type: colysActions.GIVE_ME_PLAYER_DATA }));
}

function* playSaga() {
  yield takeLatest(actions.REQUEST_PLAYER_DATA, handleSagaRequestPlayerData);
}

export { selectors, actions, playSaga };
