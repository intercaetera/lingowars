import React from "react";
import { connect } from "react-redux";
import Character from "components/Character";
import Loading from "components/Loading";
import { Button, Icon, Container, Progress } from "nes-react";
import "screens/Play/style.scss";
import { actions } from "screens/Play/reduxStuff";
import { selectors } from "redux/selectors";
import { setPlayerID } from "../SignUp/redux/actions";

import { getRoom, removeRoom } from "../../services/colyseusService";

const avatars = [
  'dude',
  'hulk',
  'shroom',
];

class PlayScreen extends React.PureComponent {
  state = {
    avatarName: null,
  };
  componentDidMount() {
    const avatarName = avatars[Math.floor(Math.random() * avatars.length)];
    this.setState({
      avatarName,
    });
    removeRoom();
  }

  joinRoom = () => {
    const { username, setPlayerID, playerID } = this.props;
    const { avatarName } = this.state;
    getRoom(username, avatarName).then(room => {
      setPlayerID(room.sessionId);
      this.props.history.push("/lobby");
    });
  };


  render() {
    const { username, loading } = this.props;
    const { avatarName } = this.state;

    if (loading) {
      return <Loading />
    }
    return (
      <div className="PlayScreen">
        <div className="InfoPanelContainer">
          <span className="UsernameContainer">
            <Container>{username}</Container>
          </span>
          <span className="StarsContainer">
            <Icon small icon="star" /> 231
          </span>
        </div>
        <div className="AvatarContainer">
          <Character avatarName={avatarName} scale={1} />
        </div>
        <div className="PlayButtonContainer">
          <Button primary className="w-80" onClick={this.joinRoom}>
            PLAY
          </Button>
        </div>
        <div className="GameOptionsContainer">
          <Button className="w-90">Character</Button>
          <Button className="w-90">Achievements</Button>
          <Button className="w-90">Shop</Button>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    playerID: selectors.getPlayerId(state),
    roomIsReady: selectors.roomIsReady(state),
    username: selectors.getPlayerName(state),
    loading: state.signup.loading,
  }),
  { joinRoom: actions.joinRoom, setPlayerID }
)(PlayScreen);
