import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Button, TextInput } from "nes-react";

import { setUsername, setPlayerID } from "./redux/actions";
import { signUpRequest } from './reduxStuff';
import logo from "../../assets/logo.png";
import "./styles.scss";

class SignUpScreen extends PureComponent {
  state = {
    username: ""
  };

  handleUsernameChange = e => this.setState({ username: e.target.value });

  handleSignUpClick = () => {
    const { username } = this.state;
    const { setUsername, signUpRequest } = this.props;
    if (!username) {
      alert("Invalid username!");
      return;
    }
    setUsername(username);
    signUpRequest();
    this.props.history.push("/play");
  };

  render() {
    const { username } = this.state;
    return (
      <div className="screen_wrapper">
        <div className="signup_logo_container">
          <img src={logo} alt="LingoWars logo" />
        </div>
        <TextInput
          placeholder="Enter your name"
          className="signup_text"
          value={username}
          onChange={this.handleUsernameChange}
        />
        <Button primary onClick={this.handleSignUpClick}>
          Sign Up
        </Button>
      </div>
    );
  }
}

export default connect(
  null,
  {
    setUsername,
    setPlayerID,
    signUpRequest,
  }
)(SignUpScreen);
