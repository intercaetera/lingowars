import { SET_USERNAME, SET_PLAYER_ID } from '../../../redux/actions';

export const setUsername = username => ({
  type: SET_USERNAME,
  payload: {
    username,
  }
});

export const setPlayerID = playerId => ({
  type: SET_PLAYER_ID,
  payload: {
    playerId,
  }
});
