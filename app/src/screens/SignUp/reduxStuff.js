/* eslint-disable no-param-reassign */

import produce from "immer";
import _ from "lodash";
import { takeLatest, call, put } from "redux-saga/effects";
import { actions } from 'redux/sagas/default';
import { SERVER_URL } from '../../globals';

export const SIGN_UP_ACTION_STARTED = 'SIGN_UP_ACTION_STARTED';
export const SIGN_UP_ACTION_SUCCESS = 'SIGN_UP_ACTION_SUCCESS';
export const SIGN_UP_ACTION_FAILIED = 'SIGN_UP_ACTION_FAILIED';

const initialState = {
  avatar: null,
  loading: false,
};

export const signUpRequest = () => ({
  type: SIGN_UP_ACTION_STARTED,
});

const handleSignupStarted = (state) => {
  state.loading = true;
};

const handleSignupSuccess = (state, { payload }) => {
  state.loading = false;
  state.stars = payload.stars;
  state.avatarName = payload.avatarName;
};

const handleSignupFailed = state => {
  state.loading = false;
};

export default function(state = initialState, action) {
  const reducers = {};

  reducers[SIGN_UP_ACTION_STARTED] = handleSignupStarted;
  reducers[SIGN_UP_ACTION_SUCCESS] = handleSignupSuccess;
  reducers[SIGN_UP_ACTION_FAILIED] = handleSignupFailed;

  if (!action || !action.type) {
    return state;
  }

  if (!reducers[action.type]) {
    return state;
  }

  return produce(state, draft => reducers[action.type](draft, action));
}

function* handleSignupAction() {
  const getUserData = () => fetch(`http://${SERVER_URL}/api/signup`)
    .then(data => data.json())
    .then(data => {
      return data;
    })
  try {

    const response = yield call(getUserData);
    yield put({ type: SIGN_UP_ACTION_SUCCESS, payload: response });
  } catch(err) {
    yield put({ type: SIGN_UP_ACTION_FAILIED });
  }
}

function* signupSaga() {
  yield takeLatest(SIGN_UP_ACTION_STARTED, handleSignupAction);
}

export { actions, signupSaga };
