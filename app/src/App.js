import React, { useEffect } from "react";
import { Provider, connect } from "react-redux";
import { IntlProvider } from "react-intl";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import store from "redux/store";

import SignUpScreen from "screens/SignUp";
import PlayScreen from "screens/Play";
import LobbyScreen from "screens/Lobby";
import BattleScreen from "screens/Battle";
import EndgameScreen from "screens/Endgame";

import { actions } from "screens/Play/reduxStuff";
import getMessages from "internationalization";

import "./globalStyles.scss";

const AppWithRedux = ({ locale, requestPlayerData }) => {
  useEffect(() => {
    requestPlayerData();
  }, [requestPlayerData]);
  return (
    <IntlProvider locale="en" messages={getMessages(locale)}>
      <Router>
        <Switch>
          <Route exact path="/" component={SignUpScreen} />
          <Route exact path="/play" component={PlayScreen} />
          <Route exact path="/lobby" component={LobbyScreen} />
          <Route exact path="/battle" component={BattleScreen} />
          <Route exact path="/end-game" component={EndgameScreen} />
        </Switch>
      </Router>
    </IntlProvider>
  );
};

const Connected = connect(
  () => ({
    // TODO retrieve it from store
    locale: "pl"
  }),
  {
    requestPlayerData: actions.requestPlayerData
  }
)(AppWithRedux);

const AppWithoutReduxYet = () => (
  <Provider store={store}>
    <Connected />
  </Provider>
);

export default AppWithoutReduxYet;
