import React from "react";
import ReactDOM from "react-dom";
import { createGlobalStyle } from "styled-components";
import * as serviceWorker from "serviceWorker";
import "antd/dist/antd.css";
import StyleVariables from "config/styles";
import App from "./App";

const GlobalStyle = createGlobalStyle`
html,
body,
#root {
  @import url("https://fonts.googleapis.com/css?family=Roboto:300,400,900&display=swap&subset=latin-ext");
  font-family: "Roboto", sans-serif;
  font-size: 100%;
  background-color: ${StyleVariables.primaryBackgroundColor};
  width: 100%;
  height: 100%;
}
`;

ReactDOM.render(
  <React.Fragment>
    <GlobalStyle />
    <App />
  </React.Fragment>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
