const styleVariables = {
  primaryTextColor: '#7952b3',
  primaryBackgroundColor: '#f0f3f5',

  mediaBreakpoints: {
    small: '576px',
    medium: '768px',
    large: '992px',
    xlarge: '1200px',
  },
};

export default styleVariables;
