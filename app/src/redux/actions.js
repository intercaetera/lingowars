import _ from "lodash";

export const ROOM_IS_READY = "ROOM_IS_READY";
export const SET_USERNAME = "SET_USERNAME";
export const SET_PLAYER_ID = "SET_PLAYER_ID";

export const setRoomIsReady = () => ({
  type: ROOM_IS_READY
});

export const setUsername = (state, { payload }) => {
  state.username = payload.username;
};

export const setPlayerId = (state, { payload }) => {
  state.playerID = payload.playerId;
};

export const setPlayersList = (state, { payload }) => {
  state.playersList = payload;
};

export const handleUserJoined = (state, { payload }) => {
  state.playersList = [...state.playersList, payload];
};

export const handleStartGame = state => {
  state.gameStarted = true;
};

export const handleBattleTick = (state, { payload }) => {
  state.playersList = payload;
};

export const handleUserLeftWhileGame = (state, { payload }) => {
  const dedPlejerIndex = _.findIndex(
    state.playersList,
    e => e.id === payload.id
  );

  if (dedPlejerIndex !== -1) {
    state.playersList[dedPlejerIndex].hp = 0;
  }
};

export const handleEndGame = (state, { payload }) => {
  state.endGame = true;
  state.winner = state.playerID === payload.winner.id;

  if(state.winner) {
    state.correctAnswersNumber = payload.winner.correctAnswersNumber;
    state.wrongAnwersNumber = payload.winner.wrongAnwersNumber;
  }
};

export const handlePlayerDead = (state, { payload }) => {
  if (payload.id === state.playerID) {
    state.iAmDead = true;
    state.correctAnswersNumber = payload.correctAnswersNumber;
    state.wrongAnwersNumber = payload.wrongAnwersNumber;
  }
};
