import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import rootReducer from "redux/reducers";
import { playSaga } from "screens/Play/reduxStuff";
import commonSaga from "./sagas/default";
import { battleSaga } from "screens/Battle/reduxStuff";
import { signupSaga } from "screens/SignUp/reduxStuff";

const sagaMiddleware = createSagaMiddleware();

// @ts-ignore
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(playSaga);
sagaMiddleware.run(commonSaga);
sagaMiddleware.run(battleSaga);
sagaMiddleware.run(signupSaga);

export default store;
