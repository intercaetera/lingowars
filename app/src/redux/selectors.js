import { createSelector } from "reselect";
import _ from "lodash";

const selectors = {
  getPlayerId: state => _.get(state, "common.playerID"),
  getPlayerList: state => _.get(state, "common.playersList"),
  roomIsReady: state => _.get(state, "common.roomReady"),
  getPlayerName: state => _.get(state, "common.username"),
  getPrompt: state => _.get(state, "battle.prompt"),
  getUserAnswer: state => _.get(state, "battle.userAnswer"),
  getFeedback: state => _.get(state, "battle.feedbackResult"),
  getIfIamDead: state => _.get(state, "common.iAmDead"),
  getNumberOfCorrect: state => _.get(state, "common.correctAnswersNumber"),
  getNumberOfWrong: state => _.get(state, "common.wrongAnwersNumber")
};

export const getPlayerAvatarName = createSelector(
  selectors.getPlayerId,
  selectors.getPlayerList,
  (currentPlayerId, playerList = []) => {
    const player = playerList.find(player => player.id === currentPlayerId);
    if (!player) {
      return null;
    }
    return player.avatarName
  }
);

export { selectors };
