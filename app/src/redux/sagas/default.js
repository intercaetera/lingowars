import { call, put, take, takeLatest } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { actions as battleActions } from "screens/Battle/reduxStuff";
import { SET_PLAYER_ID } from "../actions";
import { singletonRoom } from "services/colyseusService";

export const actions = {
  REQUEST_PLAYER_DATA: "REQUEST_PLAYER_DATA",
  SET_PLAYER_DATA: "SET_PLAYER_DATA",
  SET_PLAYERS_LIST: "SET_PLAYERS_LIST",
  USER_JOINED: "USER_JOINED",
  START_GAME: "START_GAME",
  BATTLE_TICK: "BATTLE_TICK",
  USER_LEFT_WHILE_GAME_STARED: "USER_LEFT_WHILE_GAME_STARED",
  HANDLE_END_GAME: "HANDLE_END_GAME",
  HANDLE_PLAYER_IS_DEAD: "HANDLE_PLAYER_IS_DEAD",

  requestPlayerData: () => ({ type: actions.REQUEST_PLAYER_DATA })
};

// saga
export const colysActions = {
  GIVE_ME_PLAYER_DATA: "GIVE_ME_PLAYER_DATA",
  DEAL_DAMAGE_TO_PLAYER: "DEAL_DAMAGE_TO_PLAYER"
};

const colysReactions = {
  I_GAVE_YOU_PLAYER_DATA: actions.SET_PLAYER_DATA,
  JOINING_SUCCESSFUL: actions.SET_PLAYERS_LIST,
  USER_JOINED: actions.USER_JOINED,
  START_GAME: actions.START_GAME,
  GET_QUESTION_RESPONSE: battleActions.RECEIVED_QUESTION,
  SEND_FEEDBACK: battleActions.RECEIVED_FEEDBACK,
  BATTLE_TICK: actions.BATTLE_TICK,
  USER_LEFT_WHILE_GAME_STARED: actions.USER_LEFT_WHILE_GAME_STARED,
  END_GAME: actions.HANDLE_END_GAME,
  PLAYER_IS_DEAD: actions.HANDLE_PLAYER_IS_DEAD
};

const createRoomChannel = room =>
  eventChannel(emit => {
    room.onMessage(data => {
      emit(data);
    });

    room.onStateChange(data => {
      emit(data);
    });

    return () => {};
  });

function* listenRoom() {
  const roomChannel = yield call(createRoomChannel, singletonRoom);
  while (true) {
    const payload = yield take(roomChannel);
    const { type, payload: colysPayload } = payload;
    const reactionAction = colysReactions[type];
    if (!reactionAction) {
      console.warn(`No action with type: ${type}`);
    } else {
      yield put({ type: reactionAction, payload: colysPayload });
    }
  }
}

function* saga() {
  yield takeLatest(SET_PLAYER_ID, listenRoom);
}

export default saga;
