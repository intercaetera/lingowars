/* eslint-disable no-param-reassign */

import produce from "immer";
import {
  SET_USERNAME,
  SET_PLAYER_ID,
  setUsername,
  setPlayerId,
  setPlayersList,
  handleUserJoined,
  handleStartGame,
  handleBattleTick,
  ROOM_IS_READY,
  handleUserLeftWhileGame,
  handleEndGame,
  handlePlayerDead
} from "./actions";
import { actions } from "./sagas/default";

const initialState = {
  playerID: null,
  username: null,
  roomReady: false,
  playersList: [],
  gameStarted: false,
  endGame: false,
  winner: false,
  iAmDead: false
};

const handleRoomIsReady = state => {
  state.roomReady = true;
};

export default function(state = initialState, action) {
  const reducers = {};

  reducers[ROOM_IS_READY] = handleRoomIsReady;
  reducers[SET_USERNAME] = setUsername;
  reducers[SET_PLAYER_ID] = setPlayerId;
  reducers[actions.SET_PLAYERS_LIST] = setPlayersList;
  reducers[actions.USER_JOINED] = handleUserJoined;
  reducers[actions.START_GAME] = handleStartGame;
  reducers[actions.BATTLE_TICK] = handleBattleTick;
  reducers[actions.USER_LEFT_WHILE_GAME_STARED] = handleUserLeftWhileGame;
  reducers[actions.HANDLE_END_GAME] = handleEndGame;
  reducers[actions.HANDLE_PLAYER_IS_DEAD] = handlePlayerDead;

  if (!action || !action.type) {
    return state;
  }

  if (!reducers[action.type]) {
    return state;
  }

  return produce(state, draft => reducers[action.type](draft, action));
}
