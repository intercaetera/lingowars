import { combineReducers } from "redux";
import commonReducer from "redux/common";
import battleReducer from "screens/Battle/reduxStuff";
import signupReducer from "screens/SignUp/reduxStuff";

export default combineReducers({
  common: commonReducer,
  battle: battleReducer,
  signup: signupReducer,
});
