import React, { useState } from 'react';
import { ClipLoader } from 'react-spinners';
import logo from "../assets/logo.png";
import "./Loading.scss";

export default function Loading() {
  const [value, setValue] = useState(0);
  setInterval(() => {
    if (value === 100) {
      setValue(0);
    } else {
      setValue(100);
    }
  }, 1000);
  return (
    <div className="LoadingWrapper">
      <img src={logo} className="LoadingLogo" />
      <div className="ClipLoader">
        <ClipLoader />
      </div>
      
    </div>   
  )
}