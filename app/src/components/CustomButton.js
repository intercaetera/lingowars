import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const CustomButtonEl = styled.button`
  margin-top: 1rem;
  margin-left: 1rem;
  margin-right: 1rem;
  margin-bottom: 0.5rem;
  padding-top: 0.5rem;
  text-align: center;
  font-size: 0.75rem;
  border-top: 1px solid #e8e8e8;
`;

const CustomButton = ({ handleOnClick }) => (
  <CustomButtonEl onClick={handleOnClick}>My button</CustomButtonEl>
);

CustomButton.propTypes = {
  handleOnClick: PropTypes.func.isRequired,
};

export default CustomButton;
