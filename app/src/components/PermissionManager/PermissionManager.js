import React from 'react';
import { connect } from 'react-redux';
import { Result } from 'antd';
import { Redirect } from 'react-router-dom';

const determineAccess = (requiredPermissions = [], permissions = []) => {
  if (!requiredPermissions || requiredPermissions.length === 0) {
    return true;
  }

  return requiredPermissions.every(perm => permissions.includes(perm));
};

class PermissionManager extends React.PureComponent {
  render() {
    const {
      redirectTo,
      display403,
      children,
      requiredPermissions,
      permissions,
    } = this.props;
    const hasAccess = determineAccess(requiredPermissions, permissions);

    if (!hasAccess) {
      if (redirectTo) {
        return <Redirect to={redirectTo} />;
      }

      if (display403) {
        return (
          <Result
            status="403"
            title="403"
            subTitle="Sorry, you are not authorized to access this page."
          />
        );
      }

      return null;
    }

    const childrenWithProps = React.Children.map(children, child => React.cloneElement(child));

    return <div>{childrenWithProps}</div>;
  }
}

export default connect(
  state => ({
    permissions: state.default.currentUserPermissions,
  }),
  {},
)(PermissionManager);
