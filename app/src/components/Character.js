import React, { forwardRef } from "react";
import SpriteAnimator from "react-sprite-animator";

import hulk from "assets/hulk.png";
import dude from "assets/dude.png";
import shroom from "assets/shroom.png";
import skull from "assets/skull.png";

const avatarSprites = {
  hulk,
  dude,
  shroom,
  skull,
};

const avatarWidths = {
  hulk: 120,
  dude: 100,
  shroom: 100,
  skull: 100,
};

class Character extends React.PureComponent {
  static defaultProps = {
    scale: 3
  };

  render() {
    const { avatarName, scale, className, innerRef } = this.props;

    return (
      <SpriteAnimator
        sprite={avatarSprites[avatarName]}
        width={avatarWidths[avatarName]}
        height={150}
        fps={6}
        scale={scale}
        className={className}
        ref={innerRef}
      />
    );
  }
}

export default forwardRef((props, ref) => <Character innerRef={ref} {...props} />);
